<?php
class MagnificPopup extends CWidget {
	public $id;
	public $files; //array( label => url, ... , )
	public $template = "<a href='%url%'>%label%</a>";

	public function run(){
		$this->prepareAssets();
		$html = "\n<div class='magnific-popup {$this->id}' >\n";
		foreach($this->files as $label=>$url){
			$ht = str_replace(
				array("%label%","%url%"),array($label,$url),$this->template);
			$html .= "\t$ht\n";
		}
		$html .= "</div>\n";
		echo $html;

		Yii::app()->getClientScript()->registerScript(
		"magnific_uploader_script_id",
		"
		$('.{$this->id}').magnificPopup({
			delegate: 'a',
			type: 'image',
			gallery: { enabled: true }
		});
		",CClientScript::POS_LOAD);
	}
	private function prepareAssets(){
		$localdir = rtrim(dirname(__FILE__),"/")."/assets";
		$assets = rtrim(Yii::app()->getAssetManager()->publish($localdir),"/");

		$cs = Yii::app()->getClientScript();
		$cs->registerCoreScript('jquery');

		 $cs->registerCssFile($assets."/magnific-popup.css");
		 $cs->registerScriptFile($assets."/jquery.magnific-popup.min.js");
	}	
}
